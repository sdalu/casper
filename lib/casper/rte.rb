require 'date'
require 'uri'
require 'faraday'


module Casper::Fetcher
class RTE
    include Casper::Fetcher

    API_BASE_URI  = 'https://www.services-rte.com'

    TEMPO_MAPPING = { 'BLUE'  => 'bleu',
                      'WHITE' => 'blanc',
                      'RED'   => 'rouge' }
    
    def initialize(logger: nil, retries: nil)
        @logger = logger
        @api    = Faraday.new(url: API_BASE_URI) do |f|
            f.request  :url_encoded
            f.response :json
            f.response :raise_error
            f.response :logger, @logger if @logger
        end
    end

    
    ### Provider #########################################################

    provider :rte


    ### Fetchers #########################################################

    #
    # { <Date> (sorted) => "bleu" | "blanc" | "rouge" ,
    #   ... }
    fetcher :tempo do |day|
        day    = day.to_date
        year   = day.year
        season = if day >= Date.new(year, 9, 1)
                 then year .. (year + 1)
                 else (year - 1) .. year
                 end
        
        @api.get('/cms/open_data/v1/tempo',
                 :season => "#{season.begin}-#{season.end}"
                ).body
            .dig('values').select {|k,v| k =~ /\A\d{4}-\d{2}-\d{2}\z/ }
            .transform_keys   {|k| Date.parse(k) }
            .transform_values {|v| TEMPO_MAPPING.fetch(v) {
                                   raise Casper::ParserError } }
            .sort {|(a,_), (b,_)| a <=> b }.to_h
    end

end
end
