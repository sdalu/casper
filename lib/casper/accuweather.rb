require 'date'
require 'faraday'
require 'faraday/retry'

module Casper::Fetcher
class AccuWeather
        extend Casper::Fetcher
    
    API_BASE_URI  = 'http://dataservice.accuweather.com'
     
    PRECIPITATION_TYPES  = [ 'none', 'rain', 'snow', 'mixed', 'ice' ]
    WEATHER_CODE_MAPPING = {        
         1 => 'clear',
         2 => 'few clouds',
         3 => 'few clouds',
         4 => 'scattered clouds',
         5 => 'haze',
         6 => 'broken clouds',
         7 => 'overcast clouds',
         8 => 'overcast clouds',
        11 => 'fog',
        12 => 'shower rain',
        13 => 'shower rain',
        14 => 'shower rain',
        15 => 'thunderstorm',
        16 => 'thunderstorm',
        17 => 'thunderstorm',
        18 => 'rain',
        19 => 'light snow',
        20 => 'light snow',
        21 => 'light snow',
        22 => 'snow',
        23 => 'snow',
        24 => nil, # ice
        25 => 'ice-pellets',
        26 => 'freezing-rain',
        29 => 'sleet',
        30 => nil, # hot
        31 => nil, # cold
        32 => nil, # windy
        33 => 'clear',
        34 => 'few clouds',
        35 => 'scattered clouds',
        36 => 'scattered clouds',
        37 => 'haze',
        38 => 'broken clouds',
        39 => 'shower rain',
        40 => 'shower rain',
        41 => 'thunderstorm',
        42 => 'thunderstorm',
        43 => 'light snow',
        44 => 'snow', 
    }
    

    def initialize(logger: nil)
        @logger = logger
        @api    = Faraday.new(url: API_BASE_URI) do |f|
            f.response :json
            f.response :raise_error
            f.request  :retry, :max => 2, :interval => 5,
                               :retry_statuses => [ 429 ]
            f.response :logger, @logger if @logger
        end
        @locations = {}
    end

    ### Provider #########################################################

    provider :accuweather

    
    ### Authentication ###################################################

    authentication :key do |apikey|
        @apikey = apikey
    end


    ### Fetchers #########################################################
    
    fetcher :weather do | lat:, lon: |
        scale = ->(x, f) { x && (x * f).round(2) }
        loc   = @locations[[lat,lon]] ||= begin
            @api.get('/locations/v1/cities/geoposition/search',
                     apikey: @apikey, q: [ lat, lon ].map(&:to_s).join(','))
                .body.dig('Key')
        end
        json  = @api.get("/currentconditions/v1/#{loc}", apikey: @apikey,
                        unit: 'metric', details: true, language: 'en')
                   .body.first

        { :'time'           => Time.at(json.dig('EpochTime')),
          :'temperature'    => json.dig('Temperature','Metric','Value'),
          :'humidity'       => json.dig('RelativeHumidity'),
          :'pressure'       => json.dig('Pressure','Metric','Value'),
          :'wind-direction' => json.dig('Wind','Direction','Degrees'),
          :'wind-speed'     => scale.(json.dig('Wind','Speed','Metric','Value'),
                                      1000.0 / 3600.0),
          :'wind-gust'  => scale.(json.dig('WindGust','Speed','Metric','Value'),
                                      1000.0 / 3600.0),
          :'uv-index'       => json.dig('UVIndex'),
          :'visibility'     => scale.(json.dig('Visibility','Metric','Value'),
                                      1000),
          :'cloud-cover'    => json.dig('CloudCover'),
          :'cloud-ceiling'  => json.dig('Ceiling','Metric','Value'),
          :'precipitation-type' => json.dig('PrecipitationType')&.downcase,
          :'precipitation-accumulation' => json.dig('Precip1hr','Metric','Value'),
          :'precipitation-timeslot' => 3600,
          :'weather'                => if v = json.dig('WeatherIcon')
                                           WEATHER_CODE_MAPPING.fetch(v) {
                                               raise Casper::ParserError,
                                      "weather code mapping unavailable #({v})"
                                           }
                                        end
        }
    end
end
end
