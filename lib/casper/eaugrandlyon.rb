require 'date'
require 'csv'
require 'faraday'
require 'faraday-cookie_jar'

module Casper::Fetcher
class EauGrandLyon
    include Casper::Fetcher
        
    SITE_BASE_URI  = 'https://agence.eaudugrandlyon.com'
    
    def initialize(logger: nil, retries: nil)
        @logger = logger
        @jar    = HTTP::CookieJar.new
        @site   = Faraday.new(url: SITE_BASE_URI) do |f|
            f.request  :url_encoded
            f.response :raise_error
            f.response :logger, logger if logger
            f.use      :cookie_jar, jar: @jar
        end
    end

    
    ### Provider #########################################################

    provider :eaugrandlyon


    ### Authentication ###################################################

    authentication :session do |username, password|
        @jar.clear
        @site.post('/default.aspx', :login => username, :pass => password,
                                    :connect => 'OK')
    end

    authentication :export do
        StringIO.new.tap {|io| @jar.save(io) }.read
    end

    authentication :import do |data|
        @jar.load(StringIO.new(data))
    end
    

    ### Fetchers #########################################################

    #
    # [ [ date, volume ]... ]
    #
    fetcher :monthly do |day = Date.today|
        mstr = day.strftime('%m/%Y')
        data = @site.get('/mon-espace-suivi-personnalise.aspx',
                         :ex => mstr, :mm => mstr)

        if data.status == 302 &&
           data[:location]&.start_with?("/inscription.aspx")
            raise Casper::AuthError
        end

        csv  = CSV.parse(data.body, :headers => true, :col_sep => ';')

        if csv.headers != [ "date", "consommation(litre)" ]
            raise Casper::ParserError, "unexpected CVS header format"
        end

        csv.map {|r|
            { :time     => Time.parse(r[0]),
              :duration => 86400,
              :volume   => Integer(r[1]),
            }
        }
    rescue CSV::MalformedCSVError
        raise Casper::ParserError, (data.status.to_s + ':' +
                                    data['location'] + ':' + data.body)
    end

end
end
