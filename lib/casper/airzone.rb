require 'date'
require 'uri'
require 'faraday'


module Casper::Fetcher
class AirZone
    include Casper::Fetcher

    MODES = { 1 => 'stop',
              2 => 'cooling',
              3 => 'heating',
              4 => 'fan',
              5 => 'dry',
              6 => 'auxiliary',
              7 => 'auto'
            }
    
    def initialize(base_uri, logger: nil, retries: nil)
        @logger = logger
        @api    = Faraday.new(url: base_uri) do |f|
            f.request  :url_encoded
            f.response :json
            f.response :raise_error
            f.response :logger, @logger if @logger
        end
    end

    
    ### Provider #########################################################

    provider :airzone


    ### Fetchers #########################################################

    #
    # { 
    # }
    fetcher :hvac do |system = 0, zone = 0|
        data = @api.get('/api/v1/hvac',
                        { :systemid => system, :zoneid => zone })
                   .body
        data = if data.include?('systems')
               then data.dig('systems', 0, 'data')
               else data.dig('data')
               end
        data.map {|d| normalize_hvac(d) }
    end


    #
    # { systemID       : <integer>,
    #   mc_connected   : <integer>,
    #   system_firmware: <string>,
    #   system_type    : <string>,
    #   manufacturer   : <string>,
    #   errors         : <array>,
    #   qadapt         : <integer>
    # }
    fetcher :system do
      @api.get('/api/v1/hvac', { :systemid => 127 }).body
        .dig('systems', 0)
    end
    
    #
    # { mac              : <string>,            # Device MAC.
    #   wifi_channel     : <integer>,           # WiFi channel.
    #   wifi_quality     : <integer>,           # WiFi signal quality.
    #   wifi_rssi        : <integer>,           # WiFi RSSI.
    #   interface        : 'wifi' | 'ethernet', # Connection type.
    #   ws_firmware      : <string>,            # Firmware version.
    #   lmachine_firmware: <string>,            # Gateway to machine firmware
    #   ws_type          : <string>             # Webserver type.
    # }
    fetcher :webserver do
        @api.get('/api/v1/webserver').body
    end

    private

    def normalize_hvac(json)
        data = json.dup
        if data['humidity']&.zero?
            data.delete('humidity')
        end
        if data.include?('modes')
            data['modes'] = data['modes'].map {|m|
                MODES.fetch(m) { raise "unknown mode <#{m}>" }
            }
        end
        if data.include?('mode')
            data['mode'] = MODES.fetch(data['mode']) {
                raise "unknown mode <#{m}>"
            }
        end
        if data.include?('roomTemp')
            data['temperature'] = data.delete('roomTemp')
        end
        
        demand = []
        demand << 'air'   if data.delete('air_demand'  ) == 1
        demand << 'floor' if data.delete('floor_demand') == 1
        demand << 'heat'  if data.delete('heat_demand' ) == 1
        demand << 'cold'  if data.delete('cold_demand' ) == 1
        data['demand'] = demand unless demand.empty?
        
        data.transform_keys!(&:to_sym)
    end
    
end
end
