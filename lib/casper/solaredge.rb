require 'date'
require 'faraday'
require 'faraday/retry'

module Casper::Fetcher
class SolarEdge
    extend Casper::Fetcher
    using Casper::StringShaper
    
    API_BASE_URI  = 'https://monitoringapi.solaredge.com'

    TIME_UNIT = %i[ QUARTER_OF_AN_HOUR HOUR DAY WEEK MONTH YEAR ]
    METERS    = %i[ Production Consumption SelfConsumption FeedIn Purchased ]

    OPERATION_MODE = { 0 => 'grid', 1 => 'battery', 2 => 'generator' }

    def initialize(logger: nil)
        @logger = logger
        @api    = Faraday.new(url: API_BASE_URI) do |f|
            f.response :json
            f.response :raise_error
            f.request  :retry, :max => 2, :interval => 5,
                               :retry_statuses => [ 429 ]
            f.response :logger, @logger if @logger
        end
        @locations = {}
    end

    ### Provider #########################################################

    provider :solaredge

    
    ### Authentication ###################################################

    authentication :key do |apikey|
        @apikey = apikey
    end


    ### Fetchers #########################################################

    
    #
    # [ { time (sorted)  : <Time>,
    #     duration       : <int>,
    #     energy         : <int>
    #   }, ... ]
    fetcher :energy do | site, from:, to:, tz: nil |
        json  = @api.get("/site/#{site}/energy", api_key: @apikey,
                         startDate: case from
                                    when Time, DateTime
                                        from.to_time.localtime.to_date
                                    else
                                        from
                                    end,
                           endDate: case to
                                    when Time, DateTime
                                        to.to_time.localtime.to_date
                                    else
                                        to
                                    end,
                          timeUnit: 'QUARTER_OF_AN_HOUR')
                    .body.first

        unless json.is_a?(Array)                                 &&
               json.size == 2                                    &&
               json.dig(1, 'timeUnit'  ) == 'QUARTER_OF_AN_HOUR' &&
               json.dig(1, 'unit'      ) == 'Wh'                 &&
               json.dig(1, 'measuredBy') == 'INVERTER'           &&
               json.dig(1, 'values'    ).is_a?(Array)
            raise Casper::ParserError
        end

        json.dig(1, 'values').reject {|dv| dv['value'].nil? }.map {|dv|
                  { :time     => Time.new(dv['date'], in: tz),
                    :energy   => Integer(dv['value']),
                    :duration => 15 * 60
                  }
        }.sort_by {|d| d[:time] }
    end

    #
    # { <String:meter_type> : [
    #     { time (sorted)  : <Time>,
    #         duration     : <int>,
    #         energy       : <int>
    #     }, ... ]
    # }
    fetcher :energy_details do | site, from:, to:, tz: nil |
        json  = @api.get("/site/#{site}/energyDetails", api_key: @apikey,
                         startTime: from.to_time.localtime.strftime('%F %T'),
                           endTime:   to.to_time.localtime.strftime("%F %T"),
                          timeUnit: 'QUARTER_OF_AN_HOUR')
                    .body.first

        unless json.is_a?(Array)                               &&
               json.size == 2                                  &&
               json.dig(1, 'timeUnit') == 'QUARTER_OF_AN_HOUR' &&
               json.dig(1, 'unit'    ) == 'Wh'                 &&
               json.dig(1, 'meters'  ).is_a?(Array)
            raise Casper::ParserError
        end
        
        json.dig(1, 'meters').to_h {|meter|
            [ meter['type'  ],
              meter['values'].reject {|dv| dv['value'].nil? }.map {|dv|
                  { :time     => Time.new(dv['date'], in: tz),
                    :energy   => Integer(dv['value']),
                    :duration => 15 * 60
                  }
              }.sort_by {|d| d[:time] }
            ]
        }
    end

    
    #
    # [ { time (sorted)  : <Time>,
    #     duration       : <int>,
    #     power          : <float>
    #   }, ... ]
    fetcher :power do | site, from:, to:, tz: nil|
        json  = @api.get("/site/#{site}/power", api_key: @apikey,
                         startTime: from.to_time.localtime.strftime('%F %T'),
                           endTime:   to.to_time.localtime.strftime("%F %T"))
                    .body.first

        unless json.is_a?(Array)                                 &&
               json.size == 2                                    &&
               json.dig(1, 'timeUnit'  ) == 'QUARTER_OF_AN_HOUR' &&
               json.dig(1, 'unit'      ) == 'W'                  &&
               json.dig(1, 'measuredBy') == 'INVERTER'           &&
               json.dig(1, 'values'    ).is_a?(Array)
            raise Casper::ParserError
        end

        json.dig(1, 'values').reject {|dv| dv['value'].nil? }.map {|dv|
                  { :time     => Time.new(dv['date'], in: tz),
                    :power    => dv['value'],
                    :duration => 15 * 60
                  }
        }.sort_by {|d| d[:time] }
    end

    #
    # { <String:meter_type> : [
    #     { time (sorted)  : <Time>,
    #         duration       : <int>,
    #         power          : <float>
    #     }, ... ]
    # }
    fetcher :power_details do | site, from:, to:, tz: nil |
        json  = @api.get("/site/#{site}/powerDetails", api_key: @apikey,
                         startTime: from.to_time.localtime.strftime('%F %T'),
                           endTime:   to.to_time.localtime.strftime("%F %T"))
                    .body.first

        unless json.is_a?(Array)                               &&
               json.size == 2                                  &&
               json.dig(1, 'timeUnit') == 'QUARTER_OF_AN_HOUR' &&
               json.dig(1, 'unit'    ) == 'W'                  &&
               json.dig(1, 'meters'  ).is_a?(Array)
            raise Casper::ParserError
        end
        
        json.dig(1, 'meters').to_h {|meter|
            [ meter['type'  ],
              meter['values'].reject {|dv| dv['value'].nil? }.map {|dv|
                  { :time     => Time.new(dv['date'], in: tz),
                    :power    => dv['value'],
                    :duration => 15 * 60
                  }
              }.sort_by {|d| d[:time] }
            ]
        }
    end


    #
    # { <Integer:meter_serial_number> :
    #     { <String:meter_type> : [
    #         { time (sorted)  : <Time>,
    #           energy         : <float:index>
    #         }, ... ]
    #     }
    # }
    fetcher :meters do | site, from:, to:, tz: nil |
        json  = @api.get("/site/#{site}/meters", api_key: @apikey,
                         startTime: from.to_time.localtime.strftime('%F %T'),
                           endTime:   to.to_time.localtime.strftime("%F %T"),
                         timeUnit: 'QUARTER_OF_AN_HOUR')
                    .body.first

        unless json.is_a?(Array)                                 &&
               json.size == 2                                    &&
               json.dig(1, 'timeUnit'  ) == 'QUARTER_OF_AN_HOUR' &&
               json.dig(1, 'unit'      ) == 'Wh'                 &&
               json.dig(1, 'meters'    ).is_a?(Array)
            raise Casper::ParserError
        end

        json.dig(1, 'meters').map { |m|
            data = m.transform_keys {|k| k.snakize.to_sym }
                       .slice(:meter_serial_number, :meter_type, :values)
            data[:values].reject! {|dv| dv['value'].nil? }
            data[:values].map! {|dv|
                { :time   => Time.new(dv['date'], in: tz),
                  :energy => Integer(dv['value']) } }
            { Integer(data[:meter_serial_number]) =>
              { data[:meter_type] => data[:values].sort_by {|d| d[:time] } }
            }
        }.reduce {|acc, obj|
            acc.merge(obj) {|k, o, n| o.merge(n) }
        }
        
    end

    #
    # [ { time (sorted)           : <Time>,
    #     total_active_power      : <float>,
    #     dc_voltage              : <float>,
    #     ground_fault_resistance : <float>,
    #     power_limit             : <float>,
    #     total_energy            : <int>,
    #     temperature             : <float>,
    #     inverter_mode           : <String>,
    #     operation_mode          : <String>,
    #     l1_data                 :
    #       { current        : <float>,
    #         voltage        : <float>,
    #         frequency      : <float>,
    #         apparent_power : <float>,
    #         active_power   : <float>,
    #         reactive_power : <float>,
    #         cos_phi        : <float>
    #       },
    #    ?l2_data                : { ... }
    #    ?l3_data                : { ... }
    #   }, ... ]
    fetcher :equipment do | site, sn, from:,  to:, tz: nil |
        json  = @api.get("/equipment/#{site}/#{sn}/data", api_key: @apikey,
                         startTime: from.to_time.localtime.strftime('%F %T'),
                           endTime:   to.to_time.localtime.strftime("%F %T"))
                    .body.first

        unless json.is_a?(Array)                               &&
               json.size == 2                                  &&
               json.dig(1, 'telemetries').is_a?(Array)
            raise Casper::ParserError
        end

        json.dig(1, 'telemetries').map {|t|
            date = t.delete('date')
            t['inverterMode' ] = t['inverterMode']
            t['operationMode'] = OPERATION_MODE.fetch(t['operationMode']) {
                 raise Casper::ParserError
            }
            
            t.transform_keys! {|k| k.snakize.to_sym }
            [ :l1_data, :l2_data, :l3_data ].each do |line|
                t[line]&.transform_keys! {|k|
                    k.snakize.sub(/^ac_/, '').to_sym 
                }
            end

            { :time => Time.new(date, in: tz) }.merge(t)
        }.sort_by {|d| d[:time] }
    end

    fetcher :flow do | site |
        json  = @api.get("/site/#{site}/currentPowerFlow", api_key: @apikey)
                    .body.first
    end
    
end
end
