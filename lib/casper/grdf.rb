require 'stringio'
require 'date'
require 'time'
require 'csv'
require 'uri'
require 'faraday'
require 'faraday/follow_redirects'
require 'faraday-cookie_jar'

module Casper::Fetcher
class GRDF
    extend Casper::Fetcher
        
    LOGIN_BASE_URI   = 'https://login.monespace.grdf.fr'
    API_BASE_URI     = 'https://monespace.grdf.fr'

    DOMAIN           = 'grdf.fr'
    GOTO_URI         = URI.parse('https://sofa-connexion.grdf.fr:443'  \
                                 '/openam/oauth2/externeGrdf/authorize').tap{|u|
        response_type  = {
            :code         => nil,             
            :scope        => ['openid', 'profile', 'email', 'infotravaux',
                               '/v1/accreditation', '/v1/accreditations',
                               '/digiconso/v1', '/digiconso/v1/consommations',
                               'new_meg', '/Demande.read', '/Demande.write'
                              ].join(' '),
            :client_id    => 'prod_espaceclient',
            :state        => 0,
            :redirect_uri => 'https://monespace.grdf.fr/_codexch',
            :nonce        => '7cV89oGyWnw28DYdI-702Gjy9f5XdIJ_4dKE_hbsvag',
            :by_pass_okta => 1,
            :capp         => 'meg'
        }
        u.query = URI.encode_www_form(:response_type =>
                                      URI.encode_www_form(response_type))
    }    
    LOGIN_REFERER_URI = URI.parse('https://login.monespace.grdf.fr' \
                                  '/mire/connexion').tap {|u|
        u.query = URI.encode_www_form(:goto => GOTO_URI)
    }
    
    def initialize(logger: nil, retries: nil)
        @logger = logger
        @jar    = HTTP::CookieJar.new
        @api    = Faraday.new(url: API_BASE_URI) do |f|
            f.request  :url_encoded
            f.response :json
            f.response :raise_error
            f.response :follow_redirects
            f.response :logger, @logger if @logger
            f.use      :cookie_jar, jar: @jar
        end
        @login  = Faraday.new(url: LOGIN_BASE_URI) do |f|
            f.request  :url_encoded
            f.response :json
            f.response :raise_error
            f.response :logger, @logger if @logger
            f.use      :cookie_jar, jar: @jar
        end
    end


    ### Provider #########################################################

    provider :grdf


    ### Authentication ###################################################

    authentication :session do |username, password|
        @jar.clear
        json = @login.post('/sofit-account-api/api/v1/auth', {
                               :email    => username, :password => password,
                               :goto     => GOTO_URI, :capp     => 'meg'
                           },{ :domain   => DOMAIN,
                               :Referer  => LOGIN_REFERER_URI.to_s
                           }).body

        if (state = json['state']) != 'SUCCESS'
            raise Casper::AuthError, "unsuccessful state (#{state})"
        end

        @api.get('/')
    rescue Faraday::Error => e
        raise Casper::AuthError, e.message
    end

    authentication :export do
        StringIO.new.tap {|io| @jar.save(io) }.read
    end

    authentication :import do |data|
        @jar.load(StringIO.new(data))
    end
    

    ### Fetchers #########################################################

    #
    # { pce: { date: <date>, energy: <integer>,
    #          index_start: <integer>, index_end: <integer> }, ... }
    #
    fetcher :history do |from, to = (Date.today - 1)|
        list = @api.get('/api/e-connexion/users/pce/historique-consultation')
                   .body
        raise Casper::AuthError if list.kind_of?(String)
        
        list.map! { |meter|
            { :id      => meter.dig('id'),
              :pce     => Integer(meter.dig('numPce')),
              :updated => Time.parse(meter.dig('updatedAt'))
            }
        }
        raise Casper::AuthError if list.empty?

        @api.get('/api/e-conso/pce/consommation/informatives',
                 :dateDebut  => from.strftime("%Y-%m-%d"),
                 :dateFin    => to.strftime("%Y-%m-%d"),
                 :pceList    => list.map {|info| info[:pce] })
            .body.transform_values {|info|
            info.dig('releves').map {|v|
                if v["natureReleve"       ] == "Informative Journalier" &&
                   v["qualificationReleve"] == "Mesuré"
                    { :time        => Time.parse(v['journeeGaziere']),
                      :duration    => 86400,
                      :energy      => v['energieConsomme'],
                      :index_start => v['indexDebut'],
                      :index_end   => v['indexFin']
                    }
                end
            }.compact
        }
    rescue Faraday::UnauthorizedError
        raise Casper::AuthError
    end

end
end
