require 'date'


require 'oauth2'
require 'base64'
# https://blog.scphillips.com/posts/2017/01/the-tado-api-v2/

class OAuth2Refresh < Faraday::Middleware
    KEY = 'Authorization'.freeze

    attr_reader :oauth2_token
    
    def initialize(app = nil, token = nil)
        super(app)
        @oauth2_token = token
    end
    
    def on_request(env)
        return if env.request_headers[KEY]
        
        if @oauth2_token.expired?
            api_key = Base64.encode64("#{@oauth2_token.client.id}:#{@oauth2_token.client.secret}").gsub(/[\s]/,'')
            @oauth2_token = @oauth2_token.refresh(
                :headers => { 'Authorization' => 'Basic ' + api_key }
            )
        end
        
        unless @oauth2_token.token.to_s.empty?
            env.request_headers[KEY] = %(Bearer #{@oauth2_token.token})
        end
    end
end
Faraday::Request.register_middleware(oauth2: OAuth2Refresh)


module Casper::Fetcher
class Tado
    extend Casper::Fetcher

    # Client ID used by the Tado web application
    CLIENT_ID     = 'tado-web-app'
    # Client secret used by the Tado web application
    CLIENT_SECRET = 'wZaRN7rpjn3FoNyF5IFuxg9uMzYJcvOo' \
                    'Q8QWiIqS3hfk6gLhVlG57j5YNoZL2Rtc'
    # Site used for authentication
    AUTH_BASE_URI = 'https://auth.tado.com'
    # Site used for API requests
    API_BASE_URI  = 'https://my.tado.com'

    WEATHER_CODE_MAPPING = {
        'SUN'                 => 'clear',
        'NIGHT_CLEAR'         => 'clear',
        'CLOUDY_PARTLY'       => 'few clouds',
        'CLOUDY_MOSTLY'       => 'broken clouds',
        'CLOUDY'              => 'overcast clouds',
        'NIGHT_CLOUDY'        => 'overcast clouds',
        'FOGGY'               => 'fog',
        'THUNDERSTORM'        => 'thunderstorm',
        'DRIZZLE'             => 'drizzle',
        'RAIN'                => 'rain',
        'SNOW'                => 'snow',
        'RAIN_SNOW'           => 'sleet',
        'SCATTERED_RAIN'      => 'scattered rain',
        'SCATTERED_SNOW'      => 'scattered snow',
        'SCATTERED_RAIN_SNOW' => 'scattered sleet',
    }

    TADO_MODE_MAPPING = {
        'HOME'                          => 'home',
        'AWAY'                          => 'away',
        'OVERLAY_ACTIVE'                => 'manual',
        'MEASURING_DEVICE_DISCONNECTED' => 'disconnected',
        'OPEN_WINDOW_DETECTED'          => 'open-window'
    }
    
    def initialize(logger: nil, retries: nil)
        @logger = logger

        @tclient = OAuth2::Client.new(CLIENT_ID, CLIENT_SECRET,
                                      site: AUTH_BASE_URI)
    end


    ### Provider #########################################################

    provider :tado


    ### Authentication ###################################################

    authentication :token do |username, password|
        @token = @tclient.password.get_token(username, password)
        @api = Faraday.new(url: API_BASE_URI) do |f|
            f.request  :oauth2, @token
            f.request  :url_encoded
            f.response :json
            f.response :raise_error
            f.response :logger, @logger if @logger
        end
    rescue Faraday::Error => e
        raise Casper::AuthError, e.message
    end

    authentication :export do
        @token.to_hash
    end

    authentication :import do |data|
        @token = OAuth2::AccessToken.from_hash(data)
    end
    

    ### Fetchers #########################################################

    fetcher :me do 
        @api.get("/api/v2/me").body
    rescue Faraday::UnauthorizedError
        raise Casper::AuthError
    end

    
    #
    # [ { id       : <string>,
    #     name     : <string>,
    #     email    : <string>,
    #     presence : <nil|boolean>,
    #     devices  : { id       : <string>,
    #                  name     : <string>,
    #                  presence : <nil|boolean|:unknown> }
    #   }, ... ]
    #
    fetcher :users do |home|
        @api.get("/api/v2/homes/#{home}/users").body.map {|user|
            devices = (user.dig('mobileDevices') || []).map {|device|
                { :id       => device.dig('id'  ),
                  :name     => device.dig('name'),
                  :presence => unless device.dig('location', 'stale')
                                   device.dig('location', 'atHome')
                               end
                }.compact
            }
            presence = devices.map {|d| d[:presence] }.compact
                           .reduce {|o, v| (o == v) ? o : :unknown }
            presence = :unknown if presence.nil?
            { :id       => user.dig('id'   ),
              :name     => user.dig('name' ),
              :email    => user.dig('email'),
              :presence => presence,
              :devices  => devices,
            }
        }
    rescue Faraday::UnauthorizedError
        raise Casper::AuthError
    end

    
    #
    # [ { id      : <integer>,
    #     name    : <string>,
    #     devices : [ { serial    : <string>,
    #                   type      : <string>,
    #                   battery   : 'normal' | 'low',
    #                   childlock : <boolean>,
    #                   firmware  : <string>
    #                  }, ... ]
    #   }, ... ]
    #
    fetcher :zones do |home|
        @api.get("/api/v2/homes/#{home}/zones").body.map {|zone|
            { :id => zone.dig('id'),
              :name => zone.dig('name'),
              :devices => zone.dig('devices').map {|device|
                  { :serial    => device['serialNo'        ],
                    :type      => device['deviceType'      ],
                    :battery   => device['batteryState'    ].downcase,
                    :childlock => device['childLockEnabled'],
                    :firmwware => device['currentFwVersion']
                  }
              }
            }
        }
    rescue Faraday::UnauthorizedError
        raise Casper::AuthError
    end

    
    #
    #  { serial    : <string>,
    #    type      : <string>,
    #    battery   : 'NORMAL' | 'LOW',
    #    childlock : <boolean>,
    #    firmware  : <string>
    #  }, ... ]
    fetcher :device do |serial|
        device = @api.get("/api/v2/devices/#{serial}").body
        { :serial    => device['serialNo'        ],
          :type      => device['deviceType'      ],
          :battery   => device['batteryState'    ],
          :childlock => device['childLockEnabled'],
          :firmwware => device['currentFwVersion']
        }
    end

    fetcher :zone_states do |home|
        json  = @api.get("/api/v2/homes/#{home}/zoneStates").body

        json.dig('zoneStates')
            .transform_keys   {|key | Integer(key)          }
            .transform_values {|data| normalize_state(data) }
    end
    
    #
    # { sensors : { time        : <Time>,
    #               temperature : <float>,
    #               humidity    : <float>  },
    #   target  : { time        : <Time>,
    #               mode        : <string>,
    #               temperature : <float>  },
    #   mode    : { time        : <Time>,
    #               mode        : <string> },
    #   power   : { time        : <Time>,
    #               percentage  : <float>  }
    # }
    fetcher :zone, :state do |home, zone|
        json = @api.get("/api/v2/homes/#{home}/zones/#{zone}/state").body

        normalize_state(json)
    end

    
    #
    # { sensors : [ { time        : <Time>,
    #                 duration    : <integer>,
    #                 temperature : <float>,
    #                 humidity    : <float>    }, ... ],
    #   target  : [ { time        : <Time>,
    #                 duration    : <integer>,
    #                 mode        : <string>,
    #                 temperature : <float>    }, ... ],
    #   mode    : [ { time        : <Time>,
    #                 duration    : <integer>,
    #                 mode        : <string>   }, ... ],
    #   power   : [ { time        : <Time>,
    #                 duration    : <integer>,
    #                 level       : <string>   }, ... ]
    # }
    #
    fetcher :zone, :history do |home, zone, date|
        date = date.to_date
        json = @api.get("/api/v2/homes/#{home}/zones/#{zone}/dayReport/",
                        date: date).body

        # Interval will need to be clamped, as tado
        # will extend them to 15min before and after if crossing
        # a day
        oneday = [ date.to_time, (date + 1).to_time ]
        
        # Sensors
        dt = json.dig('measuredData', 'insideTemperature', 'dataPoints')
                 .to_h {|data|
            [ Time.parse(data.dig('timestamp')).ceil.getlocal,
              { :'temperature' => data.dig('value', 'celsius') } ]
        }
        dh = json.dig('measuredData', 'humidity', 'dataPoints')
                 .to_h {|data|
            [ Time.parse(data.dig('timestamp')).ceil.getlocal,
              { :'humidity' => (data.dig('value') * 100).round(1) } ]
        }
        s = dt.merge(dh) {|k, o, n| o.merge(n) }
              .map {|k, v| { :time => k }.merge(v) }

        # Target (ON, OFF) (+temperature)
        t = json.dig('settings', 'dataIntervals').map {|data|
            from = Time.parse(data.dig('from')).ceil.getlocal.clamp(*oneday)
            to   = Time.parse(data.dig('to'  )).ceil.getlocal.clamp(*oneday)
            { :time        => from,
              :duration    => (to - from).to_i,
              :mode        => data.dig('value', 'type').downcase,
              :temperature => if data.dig('value', 'power') == 'ON'
                                      data.dig('value','temperature','celsius')
                                  end
            }.compact
        }.reject {|data| data[:duration].zero? }

        # Stripes (HOME, AWAY, OVERLAY_ACTIVE)
        m = json.dig('stripes', 'dataIntervals').map {|data|
            from = Time.parse(data.dig('from')).ceil.getlocal.clamp(*oneday)
            to   = Time.parse(data.dig('to'  )).ceil.getlocal.clamp(*oneday)
            { :time        => from,
              :duration    => (to - from).to_i,
              :mode        => TADO_MODE_MAPPING
                                .fetch(data.dig('value', 'stripeType')) { |v|
                                  raise Casper::ParserError,
                                        "unknown tado mode (#{v})"
                              }
            }.compact
        }.reject {|data| data[:duration].zero? }

        # Power (NONE, LOW, MEDIUM, HIGH)
        p = json.dig('callForHeat', 'dataIntervals').map {|data|
            from = Time.parse(data.dig('from')).ceil.getlocal.clamp(*oneday)
            to   = Time.parse(data.dig('to'  )).ceil.getlocal.clamp(*oneday)
            { :time        => from,
              :duration    => (to - from).to_i,
              :level       => data.dig('value').downcase
            }.compact
        }.reject {|data| data[:duration].zero? }

        # Weather
        w = json.dig('weather', 'condition', 'dataIntervals').map {|data|
            from = Time.parse(data.dig('from')).ceil.getlocal.clamp(*oneday)
            to   = Time.parse(data.dig('to'  )).ceil.getlocal.clamp(*oneday)

            { :time        => from,
              :duration    => (to - from).to_i,
              :temperature => data.dig('value', 'temperature', 'celsius'),
              :weather     => if v = data.dig('value', 'state')
                                  WEATHER_CODE_MAPPING.fetch(v) {
                                      raise Casper::ParserError,
                                            "unknown weather code (#{v})"
                                  }
                              end
            }.compact
        }.reject {|data| data[:duration].zero? }

        { :sensors  => s,
          :target   => t, :mode => m, :power => p,
          :weather  => w
        }
    rescue Faraday::UnauthorizedError
        raise Casper::AuthError
    end

    
    #
    # { time            : <Time>
    #   temperature     : <float>,
    #   solar-intensity : <float:percent>
    # }
    fetcher :weather do |home|
        json = @api.get("/api/v2/homes/#{home}/weather").body

        json.values.map {|v|
            Time.parse(v.dig('timestamp')).getlocal
        }.uniq => [ timestamp ]

        { :'time'            => timestamp,
          :'temperature'     => json.dig('outsideTemperature', 'celsius'),
          :'solar-intensity' => json.dig('solarIntensity', 'percentage'),
          :'weather'         => if v = json.dig('weatherState', 'value')
                                    WEATHER_CODE_MAPPING.fetch(v) {
                                        raise Casper::ParserError,
                                              "unknown weather code (#{v})"
                                    }
                                end
        }.compact
    rescue NoMatchingPatternError
        raise Casper::ParserError
    rescue Faraday::UnauthorizedError
        raise Casper::AuthError
    end


    #
    fetcher :weather, :history do |home, date|
        # Weather history is only present in a zone dayReport
        #  -> use any zone
        zone = self[:zones].fetch(home).dig(0, :id)
        self[:zone, :history].fetch(home, zone, date).dig(:weather)
    end

    private

    def normalize_state(json)
        now  = Time.now

        # Sensors
        ts_t = json.dig('sensorDataPoints', 'insideTemperature', 'timestamp')
        ts_h = json.dig('sensorDataPoints', 'humidity',          'timestamp')
        if ts_t != ts_h
            raise Casper::ParserError, "sensors timestamps differ"
        end
        s = { :time        => Time.parse(ts_t).ceil.getlocal,
              :temperature => json.dig('sensorDataPoints',
                                       'insideTemperature', 'celsius'),
              :humidity    => json.dig('sensorDataPoints',
                                       'humidity', 'percentage')
            }
        
        # Target
        t = { :time        => now,
              :mode        => json.dig('setting', 'type'),
              :temperature => if json.dig('setting', 'power') == 'ON'
                                  json.dig('setting', 'temperature', 'celsius')
                              end
            }

        # Mode
        m = { :time        => now,
              :mode        => TADO_MODE_MAPPING
                                .fetch(json.dig('tadoMode')) { |v|
                                  raise Casper::ParserError,
                                        "unknown tado mode (#{v})"
                                }
            }
        
        # Power
        p = { :time        => Time.parse(json.dig('activityDataPoints',
                                                  'heatingPower', 'timestamp'))
                                  .ceil.getlocal,
              :percentage  => json.dig('activityDataPoints',
                                       'heatingPower', 'percentage')
            }

        # Window
        w = json.dig('openWindow')

        { :sensors => s, :target   => t, :mode => m, :power => p }
    end
    
end
end
