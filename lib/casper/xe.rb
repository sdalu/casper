require 'faraday'
require 'nokogiri'
require 'bigdecimal'

module Casper::Fetcher
class XE
    extend Casper::Fetcher
    
    SITE_BASE_URI = 'https://www.xe.com/'
    
    def initialize(logger: nil, retries: nil)
        @logger = logger
        @site   = Faraday.new(url: SITE_BASE_URI) do |f|
            f.response :raise_error
            f.response :logger, @logger if @logger
        end            
    end


    ### Provider #########################################################

    provider :xe


    ### Fetchers #########################################################

    #
    # { currency : <decimal>, ... }
    #
    fetcher :currency do |date, from: 'EUR'|
        data = @site.get('/currencytables/?from=%{from}&data=%{date}' % {
                             :from => from,
                             :date => date.strftime('%Y-%m-%d'),
                         }).body
        doc  = Nokogiri::HTML(data)
        
        list = doc.css('table[class^="currencytables__Table"] tbody tr')
        if list.empty?
            raise Casper::ParserError, "no historical rate data"
        end
        
        list.map {|e|
            cur = e.css('th').text
            val = e.css('td:nth-child(3)').text.gsub(',', '')
            
            if cur !~ /^[A-Z]{3,4}$/
                raise Casper::ParserError,
                      "unexpected currency name (#{cur})"
            end
            if val !~ /^\d+\.\d+$/
                raise Casper::ParserError,
                      "unexpected rate for currency (#{cur}: #{val})"
            end

            Hash[ cur.to_sym, BigDecimal(val) ]
        }
    end

end
end
