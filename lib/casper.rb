require 'faraday/gzip'
require 'faraday/encoding'
require 'faraday/retry'
require 'faraday/follow_redirects'

module Casper
    class Error       < StandardError ; end
    class ParserError < Error         ; end
    class NetError    < Error         ; end
    class AuthError   < Error         ; end
    class QuotaError  < Error         ; end




    
    @@fetchers = {}
    def self.register(provider, klass)
        @@fetchers[provider] = klass
    end

    def self.list
        @@fetchers.keys
    end

    def self.[](provider)
        @@fetchers.fetch(provider)
    end
    

    @@logger = nil
    def self.logger=(logger)
        @@logger = logger
    end

    def self.logger
        @@logger
    end    
end


require_relative 'casper/fetcher'


module Casper
module StringShaper
    refine String do
        def snakize
            self.gsub(/([a-z\d])([A-Z])/) do
                "#{$1}_#{$2}"
            end.downcase
        end
    end
end
end
