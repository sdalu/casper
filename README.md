
## Tado

## Eau Grand Lyon

## GRDF

## OpenWeatherMap

## Tomorrow.io

# Pollutant

* PM10 [µg/m³]
* PM25 [µg/m³]
* CO   [µg/m³]
* NO2  [µg/m³]
* O3   [µg/m³]
* SO2  [µg/m³]
* NH3  [µg/m³]
* C6H6 [µg/m³]
* dust [µg/m³] (Saharan dust)
* AOD  [-]     (Atmospheric Optical Depth)
* EPA-index [0-301]
* MEP-index [0-301]


# Weather

The following informations are returned:
* time
* duration                       [s]

* humidity                       [%]  (at 2m)
* temperature                    [°C] (at 2m)
* pressure                       [hPa] (sea level)
* uv-index                       [integer]
* cloud-cover                    [%]
* wind-speed                     [m/s] (at 10m)
* wind-gust                      [m/s] (at 10m)
* wind-direction                 [degree] (at 10m)
* precipitation-type             [none,rain,snow,mixed,ice] 
* precipitation-accumulation     [mm]
* precipitation-timeslot         [s]
* ligthning                      [km⁻²·min⁻¹]
* irradiance-global-horizontal   [W/m²]
* irradiance-diffuse-horizontal  [W/m²]
* irradiance-direct-horizontal   [W/m²]
* irradiance-direct-normal       [W/m²]
* weather                        [string]

Notes: sleet = rain+snow (american sleet = ice-pellet)


https://www.eoas.ubc.ca/courses/atsc113/flying/met_concepts/01-met_concepts/01c-cloud_coverage/index.html

Weather code are based on [openweathermap](https://openweathermap.org/weather-conditions#Weather-Condition-Codes-2)
with some additions. 

(light|heavy|scattered)? thunderstorm (with (light|heavy)? (rain|drizzle|hail))?
(light|heavy|scattered)? shower? freezing-?(drizzle|rain)|sleet|snow|ice-pellets|hail
haze|dust|sand|smoke|ash
mist|(light|heavy)? rime? fog 
whirls|squall|tornado
clear  | (few|scattered|broken|overcast) clouds



* clear sky
* few clouds: 11-25%
* scattered clouds: 25-50%
* broken clouds: 51-84%
* overcast clouds: 85-100%

 Pollutant:
 ==========
~~~
{  time  : Time
   ?PM25 : Particulate matter  2.5µm   [µg/m³]
   ?PM10 : Particulate matter 10.0µm   [µg/m³]
   ?CO   : Carbon monoxyde             [µg/m³]
   ?NO   : Nitrogen monoxide           [µg/m³]
   ?NO2  : Nitrogen dioxide            [µg/m³]
   ?O3   : Ozone                       [µg/m³]
   ?SO2  : Sulfur dioxide              [µg/m³]
   ?NH3  : Ammonia                     [µg/m³]
   ?C6H6 : Benzene                     [µg/m³]
   ?dust : Saharan dust                [µg/m³]
   ?AOD  : Atmospheric Optical Depth   [-]
  }
~~~

